module.exports = function(grunt) {
	"use strict";

	var dirs = grunt.file.readJSON('directories.json');

	require('time-grunt')(grunt);

	require('load-grunt-config')(grunt, {
		loadGruntTasks: {
			pattern: ['grunt-*']
		},
		init: true,
		config: {
			pkg: grunt.file.readJSON('package.json'),
			dirs: dirs,
			scripts: grunt.file.readJSON(dirs.js + '/require.json')
		}
	});
};