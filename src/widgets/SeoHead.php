<?php

/**
 * Сеошные мета-теги в <head>
 */
class SeoHead extends Widget {
	protected $headTitle;

	public function init() {
		$this->headTitle = $this->controller->headTitle;
	}

	public function run() {
		/** @var SeoControllerBehavior $behavior */
		$behavior = $this->controller->asa('seo');

		if ($behavior !== null) {

			if ($behavior->metaTitle !== null)
				echo CHtml::tag('title', [ ], CHtml::encode($behavior->metaTitle)) . PHP_EOL;
			else
				echo CHtml::tag('title', [ ], CHtml::encode($this->headTitle)) . PHP_EOL;

			if ($behavior->metaDescription !== null)
				echo CHtml::metaTag($behavior->metaDescription, 'description') . PHP_EOL;

			foreach ($behavior->metaProperties as $name => $content)
				echo CHtml::tag('meta', [ 'property' => $name, 'content' => $content ]) . PHP_EOL;

			if (!empty($behavior->canonical)) {
				echo CHtml::linkTag('canonical', null, $behavior->canonical) . PHP_EOL;
			}
		}
	}
}
