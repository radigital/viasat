<?php
// vim: sw=4:ts=4:noet:sta:

class SActiveForm extends CActiveForm {

	public function getId($autoGenerate=true)
	{
		return 'yw-'.time();
	}

	public function run() {
		if(is_array($this->focus))
			$this->focus="#".CHtml::activeId($this->focus[0],$this->focus[1]);

		echo CHtml::endForm();
		if (!$this->enableAjaxValidation && !$this->enableClientValidation || empty($this->attributes)) {
			if ($this->focus!==null) {
				app()->clientScript->registerScript('CActiveForm#focus',"
					if(!window.location.hash)
						jQuery('".$this->focus."').focus();
				", CClientScript::POS_END);
			}
			return;
		}

		$options=$this->clientOptions;
		if (isset($this->clientOptions['validationUrl']) && is_array($this->clientOptions['validationUrl']))
			$options['validationUrl']=CHtml::normalizeUrl($this->clientOptions['validationUrl']);

		$options['attributes']=array_values($this->attributes);

		if($this->summaryID!==null)
			$options['summaryID']=$this->summaryID;

		if($this->focus!==null)
			$options['focus']=$this->focus;

		$options=CJavaScript::encode($options);
		$id=$this->id;
		app()->clientScript->registerScript(__CLASS__.'#'.$id,"jQuery('#$id').yiiactiveform($options);", CClientScript::POS_END);
	}
}