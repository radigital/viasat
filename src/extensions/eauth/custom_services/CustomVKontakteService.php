<?php
/**
 * An example of extending the provider class.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

require_once dirname(dirname(__FILE__)) . '/services/VKontakteOAuthService.php';

class CustomVKontakteService extends VKontakteOAuthService {

	// protected $scope = 'friends';

	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
			'query' => array(
				'uids' => $this->uid,
				'fields' => 'nickname, sex, bdate, city, country, timezone, photo_big',
			),
		));

		$info = $info['response'][0];

		$this->attributes['uid'] = $info->uid;
		$this->attributes['name'] = $info->first_name;
		$this->attributes['lastname'] = $info->last_name;
		$this->attributes['gender'] = $info->sex == 1 ? 'F' : 'M';
		$this->attributes['photo'] = $info->photo_big;

	}
}
