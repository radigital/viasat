<?php
class CustomFacebookService extends FacebookOAuthService {
	/**
	 * https://developers.facebook.com/docs/authentication/permissions/
	 */
	protected $scope = 'email,user_birthday,user_hometown,user_location';

	/**
	 * http://developers.facebook.com/docs/reference/api/user/
	 *
	 * @see FacebookOAuthService::fetchAttributes()
	 */
	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://graph.facebook.com/me');
		$this->attributes['uid'] = $info['id'];
		$this->attributes['name'] = $info['first_name'];
		$this->attributes['lastname'] = $info['last_name'];
		$this->attributes['gender'] = $info['gender'] == 'male' ? 'M' : 'F';
	}
}
