<?php
/**
 * @var DefaultController $this
 * @var Yii2DebugPanel[] $panels
 * @var string $tag
 */
$url = $panels['request']->getUrl();
?>
<div id="yii2-debug-toolbar">
	<?php foreach ($panels as $panel): ?>
		<?php echo $panel->getSummary(); ?>
	<?php endforeach; ?>
	<span class="yii2-debug-toolbar-toggler">›</span>
</div>
<div id="yii2-debug-toolbar-min">
	<a href="<?php echo $url; ?>" title="Open Yii Debugger" id="yii2-debug-toolbar-logo">
		<img width="29" height="30" alt="" src="<?php echo Yii2ConfigPanel::getYiiLogo(); ?>">
	</a>
	<span class="yii2-debug-toolbar-toggler">‹</span>
</div>

<style type="text/css">
	<?php echo file_get_contents(dirname(__FILE__) . '/toolbar.css'); ?>
</style>

<script>
	$(function(){
		if (window.localStorage && localStorage.getItem('yii2-debug-toolbar') == 'minimized') {
			$('#yii2-debug-toolbar').hide();
			$('#yii2-debug-toolbar-min').show();
		} else {
			$('#yii2-debug-toolbar-min').hide();
			$('#yii2-debug-toolbar').show();
		}
		$('#yii2-debug-toolbar .yii2-debug-toolbar-toggler').click(function(){
			$('#yii2-debug-toolbar').hide();
			$('#yii2-debug-toolbar-min').show();
			if (window.localStorage) {
				localStorage.setItem('yii2-debug-toolbar', 'minimized');
			}
		});
		$('#yii2-debug-toolbar-min .yii2-debug-toolbar-toggler').click(function(){
			$('#yii2-debug-toolbar-min').hide();
			$('#yii2-debug-toolbar').show();
			if (window.localStorage) {
				localStorage.setItem('yii2-debug-toolbar', 'maximized');
			}
		});

		$('#yii2-debug-toolbar a').attr('target', '_blank');
	});
</script>