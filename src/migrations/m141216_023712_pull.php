<?php

class m141216_023712_pull extends CDbMigration
{
	public function up()
	{
		$this->createTable('pull', [
			"`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
			"`name` text NOT NULL COMMENT 'Ответ'",
			"`questionId` int(11) UNSIGNED NOT NULL COMMENT 'Вопрос для ответа'",
			"`userId` int(11) UNSIGNED NOT NULL COMMENT 'Юзер, добавивший вопрос'",
			"`dateAdd` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата добавления'",
			"PRIMARY KEY (`id`)",
			"KEY `questionId` (`questionId`)",
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пул из трех ответов'");

		$this->dropColumn('answer','questionId');
		$this->dropColumn('answer','userId');
		$this->addColumn('answer','pullId', "int(11) UNSIGNED NOT NULL COMMENT 'Пулл ID'");
	}

	public function down()
	{
		$this->dropTable('pull');
		$this->addColumn('answer','questionId', "int(11) UNSIGNED NOT NULL");
		$this->addColumn('answer','userId', "int(11) UNSIGNED NOT NULL");
		$this->dropColumn('answer','pullId');

	}

}