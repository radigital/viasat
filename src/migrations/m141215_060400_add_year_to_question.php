<?php

class m141215_060400_add_year_to_question extends CDbMigration
{
	public function up()
	{
		$this->addColumn('questions','yearFrom',"int(5) UNSIGNED NOT NULL");
	}

	public function down()
	{
		$this->dropColumn('questions','yearFrom');
	}
}