<?php

class m141201_191010_init extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', [
			"`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
			"`name` varchar(50) NOT NULL COMMENT 'Имя'",
			"`lastname` varchar(50) NULL COMMENT 'Фамилия'",
			"`passwd` varchar(32) NOT NULL COMMENT 'Пароль'",
			"`email` varchar(50) NULL COMMENT 'email'",
			"`access` enum('R','M','U') DEFAULT 'U' COMMENT 'Доступ пользователя R-root,M-manager,U-user'",
			"`create` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата создания'",
			"`lastVisit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата последнего визита'",
			"PRIMARY KEY (`id`)",
			"UNIQUE KEY `email` (`email`)"
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пользователи'");

		$this->createTable('users_social', [
			"`id` int(11) NOT NULL AUTO_INCREMENT",
			"`userId` int(11) NOT NULL COMMENT 'id основного пользователя'",
			"`name` varchar(50) NOT NULL COMMENT 'Имя'",
			"`lastname` varchar(50) NOT NULL COMMENT 'Фамилия'",
			"`email` varchar(50) NOT NULL COMMENT 'email'",
			"`uid` varchar(50) NOT NULL COMMENT 'id Сети'",
			"`network` varchar(50) NOT NULL COMMENT 'Имя сети'",
			"`create` datetime NOT NULL COMMENT 'Дата создания'",
			"PRIMARY KEY (`id`)",
			"UNIQUE KEY `email` (`uid`,`network`)"
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пользователи Социальных сетей'");

	}

	public function down()
	{
		$this->dropTable('users');
		$this->dropTable('users_social');
	}
}