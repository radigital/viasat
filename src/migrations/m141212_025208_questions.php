<?php

class m141212_025208_questions extends CDbMigration
{
	public function up()
	{
		$this->createTable('questions', [
			"`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
			"`name` varchar(255) NOT NULL COMMENT 'Вопрос'",
			"`userId` int(11) UNSIGNED NOT NULL COMMENT 'Юзер, добавивший вопрос'",
			"`dateAdd` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата добавления'",
			"PRIMARY KEY (`id`)",
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Вопросы'");

		$this->createTable('answer', [
			"`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
			"`name` text NOT NULL COMMENT 'Ответ'",
			"`questionId` int(11) UNSIGNED NOT NULL COMMENT 'Вопрос для ответа'",
			"`image` varchar(255) NOT NULL COMMENT 'Картинка ответа'",
			"`userId` int(11) UNSIGNED NOT NULL COMMENT 'Юзер, добавивший вопрос'",
			"`dateAdd` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата добавления'",
			"PRIMARY KEY (`id`)",
			"KEY `questionId` (`questionId`)",
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответы'");

		$this->createTable('results', [
			"`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT",
			"`userId` int(11) UNSIGNED NOT NULL COMMENT 'Юзер'",
			"`result` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Баллы'",
			"PRIMARY KEY (`id`)",
			"KEY `userId` (`userId`)",
		], "ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Результат'");
	}

	public function down()
	{
		$this->dropTable('results');
		$this->dropTable('questions');
		$this->dropTable('answer');
	}
}