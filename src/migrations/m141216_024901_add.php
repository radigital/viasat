<?php

class m141216_024901_add extends CDbMigration
{
	public function up()
	{
		$this->addColumn('answer','right', "ENUM('Y','N') NOT NULL default 'N' COMMENT 'Правильный ответ'");
		$this->dropColumn('pull','name');
	}

	public function down()
	{
		$this->dropColumn('answer','right');
	}
}