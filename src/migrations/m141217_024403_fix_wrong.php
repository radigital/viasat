<?php

class m141217_024403_fix_wrong extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('answer','right');
		$this->addColumn('answer','wrong', "ENUM('Y','N') NOT NULL default 'Y' COMMENT 'Неправильный ответ'");
	}

	public function down()
	{
		echo "m141217_024403_fix_wrong does not support migration down.\n";
		return false;
	}

}