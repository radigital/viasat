<?php


/**
 * Файл с функциями-помощниками
 */

/**
 * Шорткат для Yii::app()
 *
 * @return CWebApplication
 */
function app() {
	return Yii::app();
}

/**
 * Возвращает компонент БД
 *
 * @return DbConnection
 */
function db() {
	return Yii::app()->db;
}

/**
 * Создание ссылки на контроллер-действие. Шорткат для {CUrlManager::createUrl}
 */
function url($route, $params = [ ], $ampersand = '&') {
	if (isset($route[0]) && $route[0] == '/')
		throw new CException('no slashes at the beginning of url');
	return app()->urlManager->createUrl($route, $params, $ampersand);
}

/**
 * Вывод на экран
 */
function dump($var, $level=3, $die = true) {
	echo '<pre>';
	CVarDumper::dump($var, $level, true);
	echo '</pre>';
	if ($die)
		die();
}
/**
 * Display a var dump in firebug console
 *
 * @param object $object Object to display
 */
function fd($object)
{
	echo '
			<script type="text/javascript">
				console.log('.json_encode($object).');
			</script>
		';
}


/**
 * Рекурсивно мерджит массивы с конфигами по хитрой схеме.
 * Если элемент массива $a - массив, а $b - null, тогда затирает этот элемент в $a
 * array $a массив A
 * array $b массив B
 */
function mergeConfigArray($a, $b) {
	$args=func_get_args();
	$res=array_shift($args);
	while (!empty($args)) {
		$next=array_shift($args);
		foreach ($next as $k => $v) {
			if (is_integer($k))
				isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
			else if (is_null($v) && isset($res[$k]) && is_array($res[$k]))
				unset($res[$k]);
			else if ($v instanceof SReplaceArray)
				$res[$k] = $v->data;
			else if (is_array($v) && isset($res[$k]) && is_array($res[$k]))
				$res[$k] = mergeConfigArray($res[$k],$v);
			else
				$res[$k]=$v;
		}
	}
	return $res;
}

function setCookies($name, $val, $days=30) {
	$cookie = new CHttpCookie($name, $val);
	$cookie->domain = app()->params['cookieDomain'];
	$cookie->expire = time() + 60*60*24*$days;
	app()->request->cookies->add($cookie->name, $cookie);
}

function clearCookies($array) {
	foreach ($array as $name) {
		if (isset(app()->request->cookies[$name])) {
			app()->request->cookies[$name] = new CHttpCookie($name, null, [
				'expire' => -1,
				'domain' => app()->params['cookieDomain']
			]);
			unset(app()->request->cookies[$name]);
		}
	}
}
