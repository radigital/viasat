<?php

	require_once(dirname(__FILE__) . '/helpers/helpers.php');

	$config = require_once(__DIR__ . '/config/console.php');
	$localConfigFilename = __DIR__ . '/config/local.php';
	$yiic = __DIR__.'/../vendor/yiisoft/yii/framework/yiic.php';

	if (file_exists($localConfigFilename))
		$config = mergeConfigArray($config, require($localConfigFilename));

	require_once($yiic);
