<?php

return [
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Виасат',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>['log'],



	// autoloading model and component classes
	'import'=> [
		'application.models.*',
		'application.components.*',
		'application.components.db.*',
		'application.components.web.*',
		'application.behaviors.*',
		'application.helpers.*',
		'application.widgets.*',
		'application.extensions.*',
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.services.*',
	],

	// application components
	'components'=>[

		'user'=>[
			'class'=>'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		],
		'loid' => [
			'class' => 'ext.lightopenid.loid',
		],
		'eauth' => [
			'class' => 'ext.eauth.EAuth',
			'popup' => true, // Use the popup window instead of redirecting.
			'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
			'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
			'services' => [ // You can change the providers and their classes.
				'facebook' => [
					// register your app here: https://developers.facebook.com/apps/
					//https://developers.facebook.com/apps/414653048693239/
					'class' => 'CustomFacebookService',
					'client_id' => '414653048693239',
					'client_secret' => 'd0e9c498159901b9ec6860e284059677',
				],
				'vkontakte' => [
					// register your app here: https://vk.com/editapp?act=create&site=1
					//https://vk.com/editapp?id=4674524&section=info
					'class' => 'CustomVKontakteService',
					'client_id' => '4674524',
					'client_secret' => 'KndjwYbB4ZrvpazC25j9',
				],
			],
		],

		'db' => [
			'class' => 'application.components.db.DbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=viasat',
			'username' => 'root',
			'password' => 'p8ftDJK8fMmpUnam',
			'charset' => 'utf8',
			'attributes' => [ PDO::MYSQL_ATTR_COMPRESS => true ],
			'enableProfiling'=>true,
			'enableParamLogging' => true,
		],
		'request' => [
			'class' => 'application.components.HttpRequest',
		],
		'urlManager' => [
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules' => [
				'ajax/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/ajax<action>',
				'ajax/<controller:\w+>/<action:\w+>/<sub:\w+>'=>'<controller>/ajax<action><sub>',
				'ajax/<controller:\w+>/<action:\w+>'=>'<controller>/ajax<action>',
				'ajax/<controller:\w+>'=>'<controller>/ajaxIndex',

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/'=>'<controller>',
			],
		],

		'errorHandler'=>[
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		],
		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
				[
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
					'enabled'=>false,
				],

				/*[
					'class'=>'CWebLogRoute',
				],*/

			],
		],
	],

	'params'=>[
		'dev' => false,
		'adminEmail'=>'furegin@yandex.ru',
        'serverName' => isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'myviasat.ru',
		'cookieDomain' =>  '.myviasat.ru',
		'rightAnswer' => 2,	// количество баллов за правильный ответ
		'maxPoints' => 20,	// максимальное количество набранных баллов
		'fb_app_id' => 414653048693239,
		'seo' => [
			'title' =>'Машина воспоминаний',
			'desc' =>'Проверь себя и узнай — жил ли ты в СССР на самом деле!',
		],
		'admUsers' => [
			'vkontakte' => [
				137503506, // Фуреев Евгений
				5438489,	// Зотов Андрей
				183053943	// ...
			]
		]
	]
];