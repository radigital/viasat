<?php

return [
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
//	'preload'=>['log'],

	'import'=>[
		'application.models.*',
		'application.components.*',
		'application.components.db.*',
		'application.widgets.*',
		//'application.extensions.*',
	],

	// application components
	'components'=>[
		'db' => [
			'class' => 'application.components.db.DbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=viasat',
			'username' => 'root',
			'password' => 'p8ftDJK8fMmpUnam',
			'charset' => 'utf8',
			'attributes' => [ PDO::MYSQL_ATTR_COMPRESS => true ],
		],
		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
			],
		],
	],
];
