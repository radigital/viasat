<?

	/**
	 * Class WebUser
	 *
	 */
class WebUser extends CWebUser {
	public $returnUrl = '/';
	public $loginUrl = '/login';
	private $_user;

	public function getData() {
		if (empty($this->_user))
			$user = User::model()->findByPk(app()->user->id);
		if (!empty($user)) {
			$this->_user = $user;
		}

		return $this->_user;
	}

	public function getIsRoot() {
		return !app()->user->isGuest && app()->user->data->access=='R';
	}

	public function getFullName() {
		return app()->user->data->fullname;
	}

}