<?php
// vim: sw=4:ts=4:noet:sta:

class DbConnection extends CDbConnection {
	public function createCommand($query = null) {
		$this->setActive(true);
		return new DbCommand($this, $query);
	}

	public function getCommandBuilder() {
		$scheme = $this->getSchema();
		if($this->_builder!==null)
			return $this->_builder;
		else
			return $this->_builder = new DbCommandBuilder($scheme);
	}
}