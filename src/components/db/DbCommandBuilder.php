<?php

class DbCommandBuilder extends CDbCommandBuilder {

	/**
	 *  Комплексная вставка в таблицу c INSERT
	 * @param string $table
	 * @param array $data
	 * @return integer number of rows affected by the execution.
	 */
	public function multipleInsert($table, array $data) {
		return $this->composeMultipleInsertCommand($table, $data)->execute();
	}

	/**
	 * Комплексная вставка в таблицу c INSERT IGNORE
	 * @param $table
	 * @param array $data
	 * @example  [
	 * 		[
	 * 			'column11' => 1,
	 * 			'column12' => 2,
	 * 			'column13' => 3,
	 * 		],
	 * [
	 * 			'column11' => 3,
	 * 			'column12' => 5,
	 * 			'column13' => 1,
	 * 		],
	 * 		...
	 * ]
	 * @return integer number of rows affected by the execution.
	 */
	public function multipleInsertIgnore($table, array $data) {
		$templates = [
			'main' => 'INSERT IGNORE INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}'
		];
		return $this->composeMultipleInsertCommand($table, $data, $templates)->execute();
	}

	/**
	 * Мультивставка на основе шаблонов
	 * @example: dbMaster()->commandBuilder->multipleInsertTpl($table, $data, ['main' => 'INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}  ON DUPLICATE KEY UPDATE id=id'])
	 * @param $table string Название таблицы
	 * @param array $data Массив с записями, которые нужно вставить
	 * @param $tpl string Шаблон
	 * @return int Количество вставленных записей
	 */
	public function multipleInsertTpl($table, array $data, $tpl) {
		if (!$tpl)
			$tpl = ['main' => 'INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}'];
		return $this->composeMultipleInsertCommand($table, $data, $tpl)->execute();
	}

}