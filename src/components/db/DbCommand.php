<?php
// vim: sw=4:ts=4:noet:sta:

class DbCommand extends CDbCommand {
	/**
	 * Выборка результатов запроса в массив по ключу
	 * @param string $keyField поле, которое будет являться ключем массива
	 * @param array $params параметры для запроса, см. {@link query}
	 * @return array список результатов запроса
	 */
	public function queryByKey($keyField, $params = [ ]) {
		$dataReader = $this->query($params);
		$result = [ ];
		while ($row = $dataReader->read())
			$result[$row[$keyField]] = $row;
		return $result;
	}

	/**
	 * Получить список значений по ключу.
	 * Первая колонка в SQL-запросе - значение, вторая - ключ
	 * @param array $params параметры для запроса, см. {@link query}
	 * @return array список результатов запроса
	 */
	public function queryColumnByKey($params = [ ]) {
		$dataReader = $this->query($params);
		$dataReader->bindColumn(1, $v);
		$dataReader->bindColumn(2, $k);
		$result = [ ];
		while ($dataReader->read()) {
			$result[$k] = $v;
		}
		return $result;
	}

	public function insertIgnore($table, $columns) {
		$params = $names = $placeholders = [ ];
		foreach ($columns as $name => $value) {
			$names[] = $this->getConnection()->quoteColumnName($name);
			if ($value instanceof CDbExpression) {
				$placeholders[] = $value->expression;
				foreach($value->params as $n => $v)
				$params[$n] = $v;
			} else {
				$placeholders[] = ':' . $name;
				$params[':' . $name] = $value;
			}
		}
		$sql = 'INSERT IGNORE INTO ' . $this->getConnection()->quoteTableName($table)
			. ' (' . implode(', ',$names) . ') VALUES ('
			. implode(', ', $placeholders) . ')';
		return $this->setText($sql)->execute($params);
	}
}