<?php
// vim: sw=4:ts=4:noet:sta:

/**
 * Базовый класс для ActiveRecord в проекте
 */
abstract class ActiveRecord extends CActiveRecord {
	/**
	 * @var CDbConnection линк для работы с базой
	 */
	protected $dbLink;

	public function __construct($scenario = 'insert') {
		$this->dbLink = app()->db;
		parent::__construct($scenario);
	}

	public static function model($model = null) {
		if (is_null($model))
			$model = get_called_class();
		$obj = parent::model($model);
		return $obj;
	}

	public function getDbConnection() {
		if ($this->dbLink instanceof CDbConnection) {
			return $this->dbLink;
		} else {
			throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		}
	}

	/**
	 * Лимит возвращаемых записей
	 */
	public function limit($limit) {
		$this->getDbCriteria()->mergeWith([
			'limit' => $limit,
		]);
		return $this;
	}

	public function orderBy($sql) {
		$this->getDbCriteria()->mergeWith([
			'order' => $sql,
		]);
		return $this;
	}

	/**
	 * пхпшный меджик-метод для геттеров.
	 * Переопределен так, что функции-геттеры будут иметь приоритет над обычными свойствами
	 */
	public function __get($name) {
		$getter = 'get'.$name;
		if (method_exists($this,$getter))
			return $this->$getter();
		return parent::__get($name);
	}

	/**
	 * пхпшный меджик-метод для сеттеров.
	 * Переопределен так, что функции-сеттеры будут иметь приоритет над обычными свойствами
	 */
	public function __set($name, $value) {
		$setter = 'set'.$name;
		if (method_exists($this, $setter)) {
			return $this->$setter($value);
		} else if (method_exists($this, 'get'.$name)) {
			throw new CException(Yii::t('yii','Property "{class}.{property}" is read only.',
				array('{class}'=>get_class($this), '{property}'=>$name)));
		}
		return parent::__set($name, $value);
	}

	public function __isset($name) {
		$getter = 'get'.$name;
		if (method_exists($this,$getter))
			return $this->$getter() !== null;
		return parent::__isset($name);
	}

	public function __unset($name) {
		$setter = 'set'.$name;
		if (method_exists($this,$setter))
			$this->$setter(null);
		return parent::__unset($name);
	}

	public function __sleep() {
		$this->dbLink = null;
		return parent::__sleep();
	}

	public function __wakeup() {
		$this->dbLink = app()->db;
	}

}