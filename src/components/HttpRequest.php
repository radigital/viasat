<?php
// vim: sw=4:ts=4:noet:sta:

/**
 * @property array originalGet Оригинальный массив $_GET без того, что туда дописывает URL-парсер
 * @property boolean isInternalReferrer Проверяет является ли реферальная ссылка внутренней или с внешнего ресурса
 * @property string contentType Content-Type HTTP-запроса
 * @property string realUserIP реальный IP-адрес пользователя с учетом прокси
 * @property string currentUrl текущий url без http://
 */
class HttpRequest extends CHttpRequest {
	private $_originalGet;

	public function init() {
		$this->_originalGet = $_GET;

		/* form _POST array from input stream if content-type is json */
		if ($this->contentType == 'application/json')
			$_POST = json_decode(file_get_contents('php://input'), true);

		parent::init();
	}

	public function getContentType() {
		$contentType = '';
		if (isset($_SERVER['CONTENT_TYPE']))
			$contentType = trim(explode(';', $_SERVER['CONTENT_TYPE'])[0]);
		return $contentType;
	}

	public function getOriginalGet() {
		return $this->_originalGet;
	}

	public function getIsInternalReferrer() {
		return $this->isInternalUrl($this->urlReferrer);
	}

	public function isInternalUrl($urlReferrer) {
		if (empty($urlReferrer))
			return false;

		$refhost = parse_url($urlReferrer, PHP_URL_HOST);
		if (empty($refhost) || $refhost == $this->serverName || substr($refhost, -strlen($this->serverName)) == $this->serverName) {
			return true;
		}
		return false;
	}

	public function getInternalReturnUrl() {
		if (isset($_GET['returnUrl']) && $this->isInternalUrl($_GET['returnUrl']))
			return $_GET['returnUrl'];
		return false;
	}

	public function getInternalReferrer() {
		if ($this->isInternalReferrer)
			return $this->urlReferrer;
		return false;
	}

	public function getIsMobileDevice() {
		$pattern = '/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/';
		if (empty($_SERVER['HTTP_USER_AGENT']))
			return false;
		return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']) > 0;
	}

	public function getRealUserIP() {
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}


	/**
	 * Отдёт имя домена второго уровня
	 * @return string
	 */
	public function getSubdomain() {
		return isset($_SERVER['HTTP_HOST']) ? substr($_SERVER['HTTP_HOST'], 0, -strlen($this->serverName) - 1) : null;
	}

	/**
	 * Возвращает параметр HTTP_HOST
	 */
	public function getServerHost() {
		return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null;
	}

	public function getCookie($name) {
		return isset(app()->request->cookies[$name]) ? app()->request->cookies[$name]->value : '';
	}

	public function updateCookie($name, $param) {
		$cookieParam = new CHttpCookie($name, $param);
		$cookieParam->expire = time() + 60 * 60 * 24 * 30; // 30 дней
		$cookieParam->domain = app()->params['cookieDomain'];
		app()->request->cookies[$name] = $cookieParam;
		unset($cookieParam);
	}
}