<?php
// vim: sw=4:ts=4:noet:sta:

/**
 * Простая защита от CSRF-атак путем проверки того
 * что запрос пришел с внутреннего ресурса
 */
class CsrfByReferrerFilter extends CFilter {
	public function preFilter($chain) {
		if (in_array(app()->request->requestType, [ 'POST', 'PUT', 'DELETE' ]) && !app()->request->isInternalReferrer) {
			throw new CHttpException(400, 'CSRF validation failed');
		}
		$chain->run();
	}
}
