<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	protected $service;

	/**
	 * Constructor.
	 * @param EAuthServiceBase $service the authorization service instance.
	 */
	public function __construct($service) {
		$this->service = $service;
	}


	public function getId()
	{
		return $this->_id;
	}
		/**
	 * Authenticates a user based on {@link username}.
	 * This method is required by {@link IUserIdentity}.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		if ($this->service->isAuthenticated) {
			$this->username = $this->service->getAttribute('name');
			/** @var UserSocial $UserSocial */
			$UserSocial = UserSocial::model()->findByAttributes([
				'uid'=>$this->service->uid,
				'network'=> $this->service->serviceName
			]);
			$firtTime = false;
			if (empty($UserSocial)) {

				if (app()->user->isGuest) {
					//Если нет авторизации и нет социального пользователя
					//создадим основного и привяжем социального
					$user = new User;
					$user->create = new CDbExpression('NOW()');
					$user->save();
					$userId = $user->id;
					$firtTime = true;
				} else {
					$userId = app()->user->id;
				}

				$UserSocial = new UserSocial;
				$UserSocial->attributes = $this->service->attributes;
				$UserSocial->userId = $userId;
				$UserSocial->network = $this->service->serviceName;
				$UserSocial->create = new CDbExpression('NOW()');
				$UserSocial->save();
			} else {
				$userId = $UserSocial->userId;
			}

			$this->_id=$userId;

			if ($UserSocial->user->name == '')
				$UserSocial->user->name = $UserSocial->name;
			if ($UserSocial->user->lastname == '')
				$UserSocial->user->lastname = $UserSocial->lastname;

			$UserSocial->user->lastVisit = new CDbExpression('NOW()');
			$UserSocial->user->update();

			if ($firtTime && isset(app()->params['admUsers']) && isset(app()->params['admUsers'][$UserSocial->network]) && in_array($UserSocial->uid, app()->params['admUsers'][$UserSocial->network])) {
				$UserSocial->user->saveAttributes(['access'=>'R']);
			}
			$this->errorCode = self::ERROR_NONE;
		}
		else {
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}
		return !$this->errorCode;
	}

}