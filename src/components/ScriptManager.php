<?php
// vim: sw=4:ts=4:noet:sta:

/**
 * Менеджер js/css, собирает, выводит, минифицирует
 */
class ScriptManager extends CComponent {
	public $scripts = [];
	public $jsCodes = [];
	public $styles = [];

	public $minify;

	public function init() {
	}

	public function render() {
	}

	public function getAssetsUrl() {
		return app()->assetManager->publish(Yii::getPathOfAlias('application.assets'));
	}

	public function registerJs($url) {
		$this->scripts[] = $url;
	}

	public function registerJsCode($code) {
		$this->jsCodes[] = $code;
	}

	public function registerCss($url) {
		$this->styles[] = $url;
	}

	public function publish() {;
		// TODO: Minification should be here
		$html = "\n";
		if ($this->minify) {
			$assetsPath = Yii::getPathofAlias('webroot') . '/assets';

			if (!empty($this->styles)) {
				$maxTime = 0;
				foreach ($this->styles as $url) {
					if (preg_match('/^(https?\:\/\/)/',$url)) {
						$file = $url;
					} else {
						$file = Yii::getPathOfAlias('webroot') . '/' . $url;
					}
					$maxTime = max($maxTime, filemtime($file));
				}
				$hash = dechex($maxTime);
				$resultUrl = app()->baseUrl . '/assets/package-' . $hash . '.css';
				$outfile = $assetsPath . '/package-' . $hash . '.css';
				if (!file_exists($outfile)) {
					$fp = fopen($outfile, 'w');
					flock($fp, LOCK_EX);
					foreach ($this->styles as $url) {
						if (preg_match('/^(https?\:\/\/)/',$url)) {
							$file = $url;
						} else {
							$file = Yii::getPathOfAlias('webroot') . '/' . $url;
						}
						fwrite($fp, file_get_contents($file));
						fwrite($fp, "\n");
					}
					flock($fp, LOCK_UN);
					fclose($fp);
				}
				$html .= CHtml::cssFile($resultUrl) ."\n";
			}

			if (!empty($this->scripts)) {
				$maxTime = 0;
				foreach ($this->scripts as $url) {
					if (preg_match('/^(https?\:\/\/)/',$url)) {
						$file = $url;
					} else {
						$file = Yii::getPathOfAlias('webroot') . '/' . $url;
					}
					$maxTime = max($maxTime, filemtime($file));
				}
				$hash = dechex($maxTime);
				$resultUrl = app()->baseUrl . '/assets/package-' . $hash . '.js';
				$outfile = $assetsPath . '/package-' . $hash . '.js';
				if (!file_exists($outfile)) {
					$fp = fopen($outfile, 'w');
					flock($fp, LOCK_EX);
					foreach ($this->scripts as $url) {
						if (preg_match('/^(https?\:\/\/)/',$url)) {
							$file = $url;
						} else {
							$file = Yii::getPathOfAlias('webroot') . '/' . $url;
						}
						fwrite($fp, file_get_contents($file));
						fwrite($fp, ";\n");
					}

					$codes = "<script>$(window).ready(function(){";
					foreach ($this->jsCodes as $code) {
						$codes.=$code. "\n";
					}
					fwrite($fp, $codes.="});</script>");
					flock($fp, LOCK_UN);
					fclose($fp);
				}
				$html .= CHtml::scriptFile($resultUrl) ."\n";
			}
		} else {
			$baseUrl = app()->baseUrl;
			foreach ($this->styles as $file) {
				if (preg_match('/^(https?\:\/\/)/',$file)) {
					$html .= CHtml::cssFile($file) . "\n";
				} else {
					$html .= CHtml::cssFile($baseUrl.'/'.$file) . "\n";
				}
			}

			foreach ($this->scripts as $file) {
				if (preg_match('/^(https?\:\/\/)/',$file)) {
					$html .= CHtml::scriptFile($file) . "\n";
				} else {
					$html .= CHtml::scriptFile($baseUrl.'/'.$file) . "\n";
				}
			}

			$codes = "<script>$(window).ready(function(){";
			foreach ($this->jsCodes as $code) {
				$codes.=$code. "\n";
			}
			$html .=$codes."});</script>";
		}
		echo $html;
	}
}