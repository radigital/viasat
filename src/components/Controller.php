<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 *
 * @property string headTitle
 */
class Controller extends CController {

	/**
	 * @var string заголовок страницы
	 */
	private $_headTitle;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=[];
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=[];

	/**
	 * @var boolean флаг, означающий является ли страница главной
	 */
	public $isMainpage = false;

	public function filters() {
		return [
			//[ 'CsrfByReferrerFilter' ],
            ['application.filters.AjaxFilter'],
		];
	}

	/**
	 * Вывод renderPartial вместе с используемым clientScript
	 */
	public function renderPartialCs($view, $data = null) {
		$output = $this->renderPartial($view, $data, true);
		app()->clientScript->render($output);
		echo $output;
	}

	/**
	 * Вывод данных в формате JSON
	 * @param array $data массив с данными
	 * @param bool $endApp завершать ли приложение
	 */
	public function renderJson($data,$endApp=true) {
		/* убираем вывод дебаг-роутов */
		foreach (app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute)
				$route->enabled = false;
		}

		header('Content-Type: application/json');
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		if ($endApp)
			app()->end();
	}



	public function processOutput($output) {
		return parent::processOutput($output);
	}



	/**
	 * Заполнение модели данными
	 *
	 * @param array $dataSource источник данных (_GET, _POST)
	 * @param CModel $model модель данных
	 * @return Boolean
	 */
	protected function populate($dataSource, CModel $model) {
		$key = get_class($model);
		if (isset($dataSource[$key])) {
			$model->attributes = $dataSource[$key];
			return true;
		}
		return false;
	}

	public function getHeadTitle() {
		if(!$title = $this->_headTitle) {
			$crumbs = array_reverse($this->breadcrumbs);
			$title = array_shift($crumbs)['title'];
			while ($part = array_shift($crumbs)['title'])
				$title .= " — {$part}";

			$title .= ($title ? ' — ' : '') . app()->name;
		}
		return $title;
	}

	public function setHeadTitle($value) {
		$this->_headTitle = $value;
	}


	public function getBreadcrumbsPrint()
	{
		$crumbs = [];

		$i=count($this->breadcrumbs);
		foreach ($this->breadcrumbs as $crumb)
		{
			$i--;
			if ((!$i && (!isset($crumb['active']) || $crumb['active']===false)) || (isset($crumb['active']) && $crumb['active']===false))
				$crumbs[] = $crumb['title'];
			elseif(isset($crumb['dataHref']))
				$crumbs[] = CHtml::tag('a', [ 'data-href' => $crumb['link']], CHtml::encode($crumb['title']));
			else
				$crumbs[] = CHtml::link($crumb['title'], $crumb['link']);
		}

		return '/ '.implode(' / ', $crumbs);
	}

	// Ajax валидация форм
	protected function userAjaxValidation($model) {
		if(isset($_POST['ajax'])){
			echo SActiveForm::validate($model);
			app()->end();
		}
	}

	public function actionError()
	{
		if ($error = app()->errorHandler->error) {
			if (app()->request->isAjaxRequest) {
				echo $error['message'];
			} else if ($view = $this->getViewFile('error'.$error['code'])) {
				$this->render('error'.$error['code'], $error);
			} else {
				$this->render('error', $error);
			}
		}
	}

}
