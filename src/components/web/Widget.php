<?php

/**
 * Базовый класс для виджетов/блоков
 * @property Controller $controller
 */
class Widget extends CWidget {
	public function getViewPath($checkTheme = false) {
		return app()->basePath . '/views/widgets';
	}
}