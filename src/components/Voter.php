<?php

/**
 * анализ голосования
 *
 * @property Array $data
 */
class Voter extends CComponent {

	private $_data = [];
	private $_points = 0;
	private $answerId = null;
	private $answer = null;
	private $_viewYears = null;
	private $yearFrom = null;

	private $_page = null;

	public function __construct (/*$points, */$yearFrom = null, $page = null) {
		//$this->points = $points;
		$this->answerId = !empty($_POST['answer']) ? (int)$_POST['answer'] : null;
		$this->yearFrom = $yearFrom;
		$this->page = $page;
	}

	/**
	 * Проверка данного ответа
	 * @return Array|void
	 */
	private function checkAnswer() {
		if ($this->answerId && $this->answer = Answer::model()->findByPk($this->answerId)) {
			$q = $this->answer->pull->question;
			if (!in_array($q->yearFrom, $this->viewYears)) {
				$this->process();

				$this->viewYears = $q->yearFrom;

				$viewYearsLine = implode(',', $this->viewYears);
				$this->setData('viewYears', $viewYearsLine);
				setCookies('viewYears', $viewYearsLine);

				if ($this->points >= app()->params->maxPoints || count($this->viewYears) >= 10) {
					return $this->finish();
				}
			}
		}
	}

	private function setNewQuestion() {
		$cmd = db()->createCommand()->select('id')->from('questions')->order('id');

		if ($this->yearFrom !== null)
			$cmd = $cmd->where('yearFrom = :yearFrom',[':yearFrom'=>$this->yearFrom]);

		if (!empty($this->viewYears)) {
			$cmd->andWhere('yearFrom NOT IN ('.implode(',', $this->viewYears).')');
		}

		if (!$ids = $cmd->queryColumn()) {
			if (count($this->viewYears)) {
				return $this->finish();
			}
			return $this->error('Вопросы еще не готовы');

		}

		shuffle($ids);
		$questionId = reset($ids);

		$model = Question::model()->with(['pulls']);

		if (!$selectedQuestion = $model->findByPk($questionId)) {
			if (count($this->viewYears)) {
				return $this->finish();
			}
			return $this->error('Вопросы еще не готовы');
		}

		if (empty($selectedQuestion->pulls)) {
			return $this->error('Вопросы еще не готовы');
		}

		$this->setData('question', static::toArrayQuestion($selectedQuestion));

		return $this->data;

	}

	protected function getPage() {
		$this->_page;
	}

	protected function setPage($page) {
		if ($page === null) {
			$page = empty($this->viewYears) ? 'index' : 'vote';
		}
		$this->_page = $page;
		$this->setData('page', $this->_page);
	}

	public function getArrayData() {
		$this->setData('viewYears', $this->viewYears);
		$this->setData('points', $this->points);

		if ($this->data['page'] === 'index')
			return $this->data;

		if ($this->points >= app()->params->maxPoints || count($this->viewYears) >= 10) {
			return $this->finish();
		}

		if ($this->checkAnswer()['page'] === 'finish')
			return $this->data;

		$this->setNewQuestion();

		return $this->data;
	}

	private function process () {
		if ($this->answer->wrong == 'N') {
			$this->points += app()->params->rightAnswer;
			$this->setData('points', $this->points);
			setCookies('points', $this->points);
		}
	}

	private function finish () {
		$this->setData('page', 'finish');
		$this->setData('result', $this->resultText);
		return $this->data;
	}

	private function error ($text) {
		$this->setData('page', 'error');
		$this->setData('error', $text);
		return $this->data;
	}


	/**
	 * @return array
	 */
	protected function getViewYears () {
		if (empty($this->_viewYears)) {
			$this->_viewYears = app()->request->getCookie('viewYears');
			$this->_viewYears = !empty($this->_viewYears) ? explode(',',$this->_viewYears) : [];
		}
		return $this->_viewYears;
	}

	protected function setViewYears ($val) {
		$this->_viewYears [] = $val;
	}

	protected function getPoints () {
		if (empty($this->_points))
			$this->_points = app()->request->getCookie('points');
		return $this->_points;
	}

	protected function setPoints ($val) {
		$this->_points = $val;
	}

	public function getData () {
		return $this->_data;
	}

	public function setData ($key, $val) {
		$this->_data[$key] = $val;
	}

	protected function getResultText() {
		switch (true) {
			case ($this->points >= 0 && $this->points < 8): return [
				'text' => 'Вы НЕ СОВЕТСКИЙ ЧЕЛОВЕК, такое ощущение, что вы пропустили всё самое интересное.',
				'share_title' => 'Я НЕ СОВЕТСКИЙ ЧЕЛОВЕК.',
				'share_desc' => 'Пройди тест от Viasat Histoty и узнай, жил ли ты в СССР.',
				'pic' => 'Viasat-teaser1.jpg'
			];
			case ($this->points >= 8 && $this->points < 14): return [
				'text' => 'Вы ЧЕЛОВЕК СОВЕТСКОЙ ЗАКАЛКИ, ориентируетесь в истории, но память надо освежать.',
				'share_title' => 'Я ЧЕЛОВЕК СОВЕТСКОЙ ЗАКАЛКИ.',
				'share_desc' => 'Пройди тест от Viasat Histoty и узнай, сколько в тебе СССР.',
				'pic' => 'Viasat-teaser2.jpg'
			];
			case ($this->points >= 14 && $this->points <= 20): return [
				'text' => 'Вы ХОДЯЧИЙ СПРАВОЧНИК ИСТОРИИ СССР, прекрасный результат, партия могла бы вами гордиться!',
				'share_title' => 'Я ХОДЯЧИЙ СПРАВОЧНИК ИСТОРИИ  СССР.',
				'share_desc' => 'Пройди тест от Viasat Histoty и узнай, жил ли ты в СССР.',
				'pic' => 'Viasat-teaser3.jpg'
			];
		}
	}

	private static function toArrayQuestion($question) {
		$res = [
			'rangeYears' => $question->rangeYears,
			'name' => $question->name,
			'id' => $question->id,
			'answers' => []
		];
		foreach ($question->pull->answers as $answer) {
			$res['answers'][$answer->id] = [
				'id' => $answer->id,
				'img' => $answer->img,
				'name' => $answer->normalizeName,
			];
			if (app()->user->isRoot)
				$res['answers'][$answer->id]['wrong'] = $answer->wrong == "Y" ? true : false;

		}
		return $res;
	}




}