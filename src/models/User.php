<?
class User extends ActiveRecord
{
	public function tableName() {
		return 'users';
	}


	public function getFullName () {
		return ($this->name!=''?$this->name:'').($this->lastname!=''?' '.$this->lastname:'');
	}

	public function getIsRoot() {
		return $this->access == 'R'?true:false;
	}

	public function getIsManager() {
		return $this->access == 'M'?true:false;
	}

	public function getIsUser() {
		return $this->access == 'U'?true:false;
	}
}