<?

	/**
	 * Class UserSocial
	 *
	 * @property User $user
	 */
class UserSocial extends ActiveRecord
{
	public function tableName() {
		return 'users_social';
	}

	public function rules() {
		return [
			['name, lastname, email, uid, network','safe'],
		];
	}

	public function relations() {
		return [
			'user' => [ self::BELONGS_TO, 'User', 'userId' ],
		];
	}
}