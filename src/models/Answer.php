<?

	/**
	 * Class Answer
	 *
	 * @property Pull $pull
	 * @property string $wrong
	 * @property string $imageUrl
	 */
class Answer extends ActiveRecord
{
	public $file;

	public static $extensions = [ 'jpg','png' ];

	public
		$id,
		$name
		,$pullId
	;

	public function tableName() {
		return 'answer';
	}
	public function relations() {
		return [
			'pull' => [ self::BELONGS_TO, 'Pull', 'pullId' ],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Ответ',
			'file' => 'Картинка',
		];
	}

	public function rules() {
		return [
			['name', 'required'],
			['name', 'safe'],
			['pullId', 'numerical', 'integerOnly' => true, 'min' => 1, 'tooSmall' => 'Выберите вопрос'],
			['file', 'file', 'allowEmpty'=>true,'types'=> self::$extensions, 'wrongType'=>'Данный тип файла загружать нельзя'],
		];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), [
			'AutoTimestampBehavior' => [
				'class' => 'AutoTimestampBehavior',
				'created' => 'dateAdd'
			],
			'imageBehavior' => [
				'class' => 'ImageBehavior',
				'imagePath' => 'assets/images/answers',
				'imageField' => 'image',
				'filenameMask' => ['id','.','image']
			],
		]);
	}

	public function getImg() {
		if (!$this->image)
			return '<div class="noimage">Нет картинки</div>';
		return '<img src="'.$this->imageUrl.'" class="image-block_img">';
	}

	public function getNormalizeName() {
		if (!$this->name)
			return '';
		return preg_replace('/(\d+)/','<span class="image-block_data">$1</span>',$this->name);
	}


}