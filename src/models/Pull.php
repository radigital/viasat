<?
class Pull extends ActiveRecord
{

	public
		$id,
		$answer1,$file1,
		$answer2,$file2,
		$answer3,$file3
		,$wrong
	;

	public function tableName() {
		return 'pull';
	}
	public function relations() {
		return [
			'user' => [ self::BELONGS_TO, 'User', 'userId' ],
			'question' => [ self::BELONGS_TO, 'Question', 'questionId' ],
			'answers' => [ self::HAS_MANY, 'Answer', 'pullId' ],
		];
	}

	public function defaultScope() {
		return [
			'alias' => 'pull',
			'with'	=> 'answers'
		];
	}

	public function rules() {
		return [
			['answer1,answer2,answer3,wrong', 'required'],
			['answer1,answer2,answer3,questionId,wrong', 'safe'],
			['questionId', 'numerical', 'integerOnly' => true, 'min' => 1, 'tooSmall' => 'Выберите вопрос'],
			['file1,file2,file3', 'file', 'allowEmpty'=>true,'types'=> Answer::$extensions, 'wrongType'=>'Данный тип файла загружать нельзя'],
		];
	}

	public function scopes() {
		return [

		];
	}

	public function getListAnswers() {
		$res = [];
		foreach ($this->answers as $answer) {
			$res[] = CHtml::link($answer->id, app()->createAbsoluteUrl('questions/answer', [ 'id' => $answer->id ]), ['class' => $answer->wrong=='Y' ? 'wrong' : '']);
		}
		return implode(', ', $res);
	}
}