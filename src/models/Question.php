<?
class Question extends ActiveRecord
{

	const	YEARFROM = 1962,
			YEARTO = 1991,
			STEP = 3;

	public
		$id,
		$name;

//	public $pull;

	public function tableName() {
		return 'questions';
	}
	public function relations() {
		return [
			'user' => [ self::BELONGS_TO, 'User', 'userId' ],
			'pulls' => [ self::HAS_MANY, 'Pull', 'questionId' ],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Вопросы'
		];
	}

	public function rules() {
		return [
			['name', 'required'],
			['name', 'safe']
		];
	}

	public function defaultScope() {
		return [
			'alias' => 'q'
		];
	}
/*
	public function excludePulls($exclude) {
		$this->dbCriteria->addNotInCondition('pull.id',$exclude);
		return $this;
	}*/

	/*public function excludeYear($exclude) {
		$this->dbCriteria->addNotInCondition('q.yearFrom',$exclude);
		return $this;
	}*/

	public function search() {
		$criteria = new CDbCriteria;
		$criteria->order = 'yearFrom';

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
			'sort' => false,
			'pagination' => [
				'pageVar' => 'page',
				'pageSize' => 20
			],
		]);
	}

	/*public function getListAnswers() {
		if (empty($this->answers))
			return '<нет ответов на этот вопрос>';
		$res = [];
		foreach ($this->answers as $answer) {
			$res[] = CHtml::link($answer->id, app()->createAbsoluteUrl('questions/answer', [ 'id' => $answer->id ]));
		}
		return implode(', ', $res);
	}*/

	public function formatData() {
		$res = '<h4>'.$this->name.'</h4>';
		$res .= '<div class="pulls">'.$this->getBlocksPulls().'</div>';
		return $res;
	}

	public function getBlocksPulls() {
		if (empty($this->pulls))
			return '<нет пула ответов на этот вопрос>';
		$res = '';
		/** @var Pull $pull */
		foreach ($this->pulls as $pull) {
			$res .= CHtml::tag('div',['class'=>'pull','data-id'=>$pull->id], $pull->getListAnswers());
		}
		return $res;
	}

	public function getButtons() {
		return	(count($this->pulls)>=20 ? '' : (CHtml::link(CHtml::tag("span",['class'=>'glyphicon glyphicon-plus','aria-hidden'=>"true"],''), app()->createUrl('questions/pullAdd',['questionId'=>$this->id]), ['title'=>'Добавить ответы в этот вопрос']).' ')).
		CHtml::tag("span",['class'=>'question-delete glyphicon glyphicon-minus','aria-hidden'=>"true", 'data-id'=>$this->id, 'title'=>'Удалить вопрос с ответами'],'');
	}

	/**
	 * @return array
	 */
	public static function getRandomizeYearsFrom() {
		$array = self::getYearsFrom();
		shuffle($array);
		return $array;
	}

	/**
	 * @return array
	 */
	public static function getYearsFrom() {
		$array = [];
		for ($i = self::YEARFROM; $i <= self::YEARTO;$i+=self::STEP) {
			$array[$i] = $i.' - '.($i+self::STEP-1);
		}
		return $array;
	}

	public function getYearTo() {
		return $this->yearFrom + Question::STEP-1;
	}

	public function getRangeYears() {
		return $this->yearFrom . ' - ' . $this->yearTo;
	}

	/**
	 * Выбирает один пулл вопросов
	 * @return mixed
	 */
	public function getPull () {
		$idx = 0;
		$countPull = count($this->pulls);
		if ($countPull > 1) {
			$idx = rand(0,$countPull-1);
		}
		return $this->pulls[$idx];
	}

	/**
	 * Получение массива вопросов в формате <id> => <вопрос>
	 * @param bool $empty Массив вопросов с пустым полем
	 * @return array
	 */
	public static function getQuestionList($empty = false){
		$items = self::model()->findAll([
			'select' => '`id`,`name`'
		]);

		$list = CHtml::listData($items, 'id', 'name');

		if ($empty) {
			$list[0] = 'Нет вопроса';
			ksort($list);
		}
		return $list;
	}
}