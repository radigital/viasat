<div class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?=app()->request->getBaseUrl(true)?>"><?= CHtml::encode(app()->name)?></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="/questions">Вопросы</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=app()->user->data->fullName?>
						<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="<?=url('login/logout')?>">Выйти</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>