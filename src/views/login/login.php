<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */


	if (app()->user->isGuest) {
		$this->pageTitle = app()->name . ' - Вход';
		app()->eauth->renderWidget();
	} else {
		if (app()->user->isRoot) { ?>
			<div class="user-panel">
				<div class="title">Вы вошли как:</div>
				<div class="fullname"><?=app()->user->fullname?></div>
				<div class="user-panel-logout">
					<a href="<?=url('login/logout')?>">Выйти</a>
				</div>
			</div>
		<? }
	}