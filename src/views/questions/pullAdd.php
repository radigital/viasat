<?
	/**
	 * @var $this QuestionsController
	 * @var $model Answer
	 */
?>
<div class="container-fluid">
	<div class="well answers add">
		<?
			$this->pageTitle = ($model->id ? 'Редактировать':'Добавить').' ответ';
			$form = $this->beginWidget('SActiveForm', [
				'action' => app()->createUrl('questions/'. ($model->id ? 'pullEdit/'.$model->id:'pullAdd')),
				'method' => 'POST',
				'htmlOptions' => [ 'autocomplete' => "off",'enctype' => "multipart/form-data" ]
			]);
		?>
		<div class="form-group<?= $model->hasErrors('questionId') ? ' has-error' : '' ?>">
			<label for="quest">Ответ на вопрос: </label>
			<?= CHtml::activeDropDownList($model, 'questionId', Question::getQuestionList(true), ['options'=>[$questionId => ['selected'=>true]]]); ?>
			<?= $form->error($model,'questionId'); ?>
		</div>

			<?
				for ($i=1; $i<=3; $i++) {?>

					<div class="answer">
						<div class="form-group<?= $model->hasErrors('answer'.$i) ? ' has-error' : '' ?>">
							<?= CHtml::activeTextArea($model, 'answer'.$i, [ 'class' => 'form-control', 'rows'=>3, "placeholder" => "Введите ответ" ]) ?>
							<?= $form->error($model,'answer'.$i); ?>
							<div class="radio">
								<label>
									<input value="<?=$i?>" type="radio" name="Pull[wrong]"> Неправильный ответ
								</label>
							</div>
						</div>

						<? if ($model->id){?>
							<div class="image"><?=$model->img?></div>
						<?}?>
						<div class="form-group<?= $model->hasErrors('file'.$i) ? ' has-error' : '' ?>">
							<label class="control-label" for="Pull_file2">Картинка <?=$i?></label>
							<?= $form->fileField($model, 'file'.$i, [ 'class' => 'form-control' ]) ?>
							<?= $form->error($model,'file'.$i); ?>
						</div>

					</div>

			<?}?>
			<? if ($model->id){
				echo CHtml::activeHiddenField($model, 'id', [ 'value' => $model->id ]);
			}?>

		<button type="submit" class="btn btn-default"><?=$model->id ? 'Сохранить':'Добавить'?></button>
		<?$this->endWidget();?>
	</div>
</div>