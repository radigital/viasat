<div class="container-fluid">
	<div class="filter well">
		<?
			$this->pageTitle = 'Список вопросов';
			$form = $this->beginWidget('SActiveForm', [
				'action' => app()->createUrl('questions/add'),
				'method' => 'POST',
				'htmlOptions' => [ 'autocomplete' => "off" ]
			]);
		?>
			<div class="form-group<?= $model->hasErrors('name') ? ' has-error' : '' ?>">
				<label for="quest">Новый вопрос</label>
				<?= CHtml::activeTextArea($model, 'name', [ 'class' => 'form-control', 'rows'=>3, "placeholder" => "Введите вопрос" ]) ?>
				<?= $form->error($model,'name'); ?>
			</div>
			<div class="form-group<?= $model->hasErrors('year') ? ' has-error' : '' ?>">
				<label for="quest">Начальный год</label><br>
				<?= CHtml::dropDownList('yearFrom', Question::YEARFROM, Question::getYearsFrom()); ?>
				<?= $form->error($model,'year'); ?>
			</div>
			<button type="submit" class="btn btn-default">Добавить</button>
		<?$this->endWidget();?>
	</div>
</div>


<?
	$this->widget('zii.widgets.grid.CGridView', [
		'id' => get_class($this).'-grid',
		'ajaxUpdate' => false,
		'dataProvider' => $model->search(),
		'columns' => [
			[
				'header' => 'Год',
				'type' => 'raw',
				'value' => '$data->rangeYears',
			],
			[
				'type' => 'raw',
				'value' => '$data->formatData()',
			],

			[
				'header' => '',
				'type' => 'raw',
				'value' => '$data->buttons',
			]
		],
	]);