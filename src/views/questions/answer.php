<?
/**
 * @var $this QuestionsController
 * @var $answer Answer
 */
?>
<div class="container-fluid">
	<div class="well answer view">
		<h3 class="question"><?=$answer->pull->question->name?></h3>
		<?
			$form = $this->beginWidget('SActiveForm', [
				'action' => app()->createUrl('questions/answer/'.$answer->id),
				'method' => 'POST',
				'htmlOptions' => [ 'autocomplete' => "off",'enctype' => "multipart/form-data" ]
			]);
		?>
		<div class="form-group<?= $answer->hasErrors('name') ? ' has-error' : '' ?>">
			<?= CHtml::activeTextArea($answer, 'name', [ 'class' => 'form-control', 'rows'=>3, "placeholder" => "Введите ответ" ]) ?>
			<?= $form->error($answer,'name'); ?>
		</div>

		<div class="image"><?=$answer->img?></div>

		<div class="form-group<?= $answer->hasErrors('file') ? ' has-error' : '' ?>">
			<label class="control-label" for="file">Картинка</label>
			<?= $form->fileField($answer, 'file', [ 'class' => 'form-control' ]) ?>
			<?= $form->error($answer,'file'); ?>
		</div>
		<?=CHtml::activeHiddenField($answer, 'id', [ 'value' => $answer->id ])?>
		<button type="submit" class="btn btn-default">Сохранить</button>

		<?$this->endWidget();?>
	</div>
</div>