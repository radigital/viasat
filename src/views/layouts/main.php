<!DOCTYPE html>
<html lang="ru" xmlns:og="http://ogp.me/ns#">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<? $this->widget('SeoHead') ?>
	<link rel="shortcut icon" href="/favicon.ico" >
	<link rel="stylesheet" type="text/css" href="/assets/css/styles.css" />
	<?	if (app()->user->isRoot) {?>
		<link rel="stylesheet" type="text/css" href="/assets/css/stylesAdm.css" />
	<?}

	if (YII_DEBUG) {?>
		<script type="text/javascript" src="/assets/js/extra.min.js"></script>
		<script type="text/javascript" src="/assets/js/libs.min.js"></script>
		<script type="text/javascript" src="/assets/js/app.min.js"></script>
	<?} else { ?>
		<script type="text/javascript" src="/assets/js/js.min.js"></script>
	<?}?>


</head>
<body>

<?
	if (app()->user->isRoot)
		$this->widget('MainMenuWidget');
?>
<?= $content ?>

<div class="footer">

</div>
<? if (!YII_DEBUG) $this->widget('YandexMetrika',['id'=>27530439]) ?>
</body>
</html>