<div class="page-wrapper">
	<div id="page-content">
		<div class="music-button">
			<input id="cfirst" type="checkbox" name="first" hidden="">
			<label class="music-button__text" for="cfirst">звук</label>
			<audio id="music" autoplay loop>
				<source src="<?=app()->createAbsoluteUrl('assets/music/music.mp3')?>">
				<source src="<?=app()->createAbsoluteUrl('assets/music/music.ogg')?>">
				Ваш браузер не поддерживает <code>audio</code> контент.
			</audio>
		</div>
		<div id="pages">
			<? if (isset($page) && $page == 'vote') {?>
				<div class="wrapper-inner inner-item">
					<div class="inner inner-page">
						<div class="ussr-logo"></div>
						<div class="years main-title"><?=$question['rangeYears']?></div>
						<div class="years-comment main-title">года</div>
						<div class="main-subtitle subtitle-question"><?=$question['name']?></div>
						<div class="image-block" data-question-id="<?=$question['id']?>">
							<? foreach ($question['answers'] as $answer) {?>
								<div class="image-block_inner i-inline">
									<a href="#" class="image-block_image" data-answer-id="<?=$answer['id']?>">
										<?=$answer['img']?>
									</a>
									<div class="image-block_text moloko-text<?= isset($answer['wrong']) && $answer['wrong'] ? ' wrong' : ''?>"><?=$answer['name']?></div>
								</div>
							<?}?>
						</div>

					</div>
				</div>
			<? } else if (isset($page) && $page == 'finish') {?>
				<div class="wrapper-inner">
					<div class="inner inner-page">
						<div class="finish-page">
							<div class="congr-block">
								<div class="finish-title">Поздравляем!</div>
								<div class="finish-subtitle">вы набрали <?=$points?> из <?=app()->params->maxPoints?> баллов</div>
								<div class="finish-resuilt i-inline"><?=$result['text']?></div>
								<div class="finish-share">Поделись результатами с друзьями в социальных сетях!</div>
								<div class="finish-share-foto"><img src="<?=app()->getBaseUrl(true).'/assets/images/'.$result['pic']?>" alt="<?=$result['text']?>"></div>
								<div class="finish-social i-inline" data-url="<?=app()->getBaseUrl(true)?>" data-img="<?=app()->getBaseUrl(true).'/assets/images/'.$result['pic']?>" data-title="<?=$result['share_title']?>" data-desc="<?=$result['share_desc']?>">
									<a href="#" class="social-icon social-vk i-inline"></a>
									<a href="#" class="social-icon social-fb i-inline"></a>
									<a href="#" class="social-icon social-tw i-inline"></a>
								</div>

								<div class="finish-share finish-share-position">
									или попробуйте <a href="#" class="finish-share_link">пройти тест</a> еще раз!
								</div>
							</div>
						</div>
					</div>
				</div>
			<?} else {?>
				<div class="wrapper" id="page-index">
					<div class="wrapper-helper">
						<div class="inner">
							<div class="lever-text moloko-text">Поверните рычаг,
								чтобы выбрать случайный интервал!</div>
							<div class="range-text moloko-text">Потяните за ползунки
								<br />и установите необходимый диапазон</div>

							<div class="button-text moloko-text">Нажмите на кнопку, чтобы посмотреть результат!</div>
							<a href="#" class="logo"></a>

							<div class="main-subtitle">Проверь себя и узнай — жил ли ты	в СССР на самом деле!</div>
							<div class="main-title">Машина<br />воспоминаний</div>
							<div class="slider-position">
								<div id="slider"></div>
							</div>
							<form action="/vote/" method="POST">
								<div class="tablo">
									<input class="input-range tablo-cifra_1" type="text" name="amountFrom" id="amountFrom" readonly >
									<input class="input-range tablo-cifra_2" type="text" name="amountTo" id="amountTo" readonly >
									<input class="main-button" name="main" data-name="main" type="submit" value="">
									<input class="handle-button" name="handle" data-name="handle" type="submit" value="">
								</div>
							</form>
						</div>
					</div>
				</div>
			<?}?>

		</div>
	</div>
</div>
<div class="page-buffer"></div>
<div class="page-footer">
	<a class="logo-footer" href="http://deltaclick.ru/" target="_blank"><img src="assets/images/logo_deltaclick.png"></a>
</div>

<script id="page-vote" type="text/x-handlebars-template">
	<div class="wrapper-inner inner-item">
		<div class="inner inner-page">
			<div class="ussr-logo"></div>
			<div class="years main-title">{{question.rangeYears}}</div>
			<div class="years-comment main-title">года</div>
			<div class="main-subtitle subtitle-question">{{{question.name}}}</div>
			<div class="image-block" data-question-id="{{question.id}}">
				{{#each question.answers}}
				<div class="image-block_inner i-inline">
					<a href="#" class="image-block_image" data-answer-id="{{id}}">
						{{{img}}}
					</a>
					<div class="image-block_text moloko-text{{#if wrong}} wrong{{/if}}">{{{name}}}</div>
				</div>
				{{/each}}
			</div>

		</div>
	</div>
</script>

<script id="page-finish" type="text/x-handlebars-template">
	<div class="wrapper-inner">
		<div class="inner inner-page">
			<div class="finish-page">
				<div class="congr-block">
					<div class="finish-title">Поздравляем!</div>
					<div class="finish-subtitle">вы набрали {{result.points}} из <?=app()->params->maxPoints?> баллов</div>
					<div class="finish-resuilt i-inline">{{result.result.text}}</div>
					<div class="finish-share">Поделись результатами с друзьями в социальных сетях!</div>
					<div class="finish-share-foto"><img src="<?=app()->getBaseUrl(true).'/assets/images/'?>{{result.result.pic}}" alt="{{result.result.text}}"></div>
					<div class="finish-social i-inline" data-url="<?=app()->getBaseUrl(true)?>" data-img="<?=app()->getBaseUrl(true).'/assets/images/'?>{{result.result.pic}}" data-title="{{result.result.share_title}}" data-desc="{{result.result.share_desc}}">
						<a href="#" class="social-icon social-vk i-inline"></a>
						<a href="#" class="social-icon social-fb i-inline"></a>
						<a href="#" class="social-icon social-tw i-inline"></a>
					</div>

					<div class="finish-share finish-share-position">
						или попробуйте <a href="#" class="finish-share_link">пройти тест</a> еще раз!
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
