/**
 * Общие функиции JS:
 *
 *	Содержание:
 *		- plural
 *		- isEmpty
 */

/**
 * Склонение по числу
 *
 * @param n
 * @param str0
 * @param str1
 * @param str2
 * @returns {*}
 */
function plural(n, str0, str1, str2) {
	n = Number(n);
	return n%10==1&&n%100!=11?str1:(n%10>=2&&n%10<=4&&(n%100<10||n%100>=20)?str2:str0);
}

/**
 * Проверка на пустоту
 * @param v
 * @returns {boolean}
 */
function isEmpty(v) {
	var t = typeof v;
	return t === 'undefined' || ( t === 'object' ? ( v === null || Object.keys( v ).length === 0 ) : ~[false, 0, "", "0"].indexOf( v ) );
}

/**
 * Скрытые ссылки
 */
function hiddenLinkReplace() {
	$('a[data-href]').each(function(){
		$(this).attr('href', $(this).data('href'));
	});
	$('img[data-src]').each(function(){
		$(this).attr('src', $(this).data('src'));
	});
	$('iframe[data-src]').each(function(){
		$(this).attr('src', $(this).data('src'));
	});
}

function number_format (number, decimals, dec_point, thousands_sep) {
	// *     example 1: number_format(1234.56);
	// *     returns 1: '1,235'
	// *     example 2: number_format(1234.56, 2, ',', ' ');
	// *     returns 2: '1 234,56'
	// *     example 3: number_format(1234.5678, 2, '.', '');
	// *     returns 3: '1234.57'
	// *     example 4: number_format(67, 2, ',', '.');
	// *     returns 4: '67,00'
	// *     example 5: number_format(1000);
	// *     returns 5: '1,000'
	// *     example 6: number_format(67.311, 2);
	// *     returns 6: '67.31'
	// *     example 7: number_format(1000.55, 1);
	// *     returns 7: '1,000.6'
	// *     example 8: number_format(67000, 5, ',', '.');
	// *     returns 8: '67.000,00000'
	// *     example 9: number_format(0.9, 0);
	// *     returns 9: '1'
	// *    example 10: number_format('1.20', 2);
	// *    returns 10: '1.20'
	// *    example 11: number_format('1.20', 4);
	// *    returns 11: '1.2000'
	// *    example 12: number_format('1.2000', 3);
	// *    returns 12: '1.200'
	// *    example 13: number_format('1 000,50', 2, '.', ' ');
	// *    returns 13: '100 050.00'
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

function curFrmt(number, dig){
	return number_format (number, 0, ',', ' ')+(!dig ? ' руб.': '');
}

/**
 * возвращает cookie если есть или undefined
 * @param name
 * @returns {string}
 */
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined
}

/**
 * устанавливает cookie
 * @param name
 * @param value
 * @param props
 *
 * props {
 *  expires - Время истечения cookie.
 *  path - Путь для cookie.
 *  domain - Домен для cookie.
 *  secure - Пересылать cookie только по защищенному соединению.
 * }
 */
function setCookie(name, value, props) {
	props = props || {}
	var exp = props.expires
	if (typeof exp == "number" && exp) {
		var d = new Date()
		d.setTime(d.getTime() + exp*1000)
		exp = props.expires = d
	}
	if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }
	props.domain = '.' + window.location.host.replace(/([\w\d]+\.)?([\w\d]+)\.([\w\d]+)/,'$2.$3');
	value = encodeURIComponent(value)
	var updatedCookie = name + "=" + value
	for(var propName in props){
		updatedCookie += "; " + propName
		var propValue = props[propName]
		if(propValue !== true){ updatedCookie += "=" + propValue }
	}
	document.cookie = updatedCookie
}

/**
 * удаляет cookie
 * @param name
 */
function deleteCookie(name) {
	setCookie(name, null, { expires: -1, path: '/' })
}