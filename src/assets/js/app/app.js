$(function(){

	var voteApp = function () {
		this.steps = ['index','vote','finish','error'];
		this.sliderConf = {
			value:100,
			min: 1962,
			max: 1991,
			step: 3,
			slide: function( event, ui ) {
				pages.find("#amountFrom").val(ui.value );
				pages.find("#amountTo").val(parseInt(ui.value) + 2);
			}
		},
		this.mute = false;
		this.points = 0;
		this.viewYears = [];

		this.indexPageContent = '';

		this.curPage = 'index';

		this.init = function () {
			var audio = document.getElementById('music');
			if (audio)
				this.setMute(audio);

			if (pages.find('#page-index').length)
				this.curPage = 'index';
		};
	};

	voteApp.prototype = {

		setMute : function (audio) {
			this.mute = getCookie('mute') === 'true';
			if (this.mute) {
				$( ".music-button #cfirst" ).click();
				this.setMuteOn(audio);
			}
		},

		//Выключение музыки
		setMuteOn : function (audio) {
			audio.pause();
		},
		// Включение музыки
		setMuteOff : function (audio) {
			audio.play();
		},

		// установить главную страницу
		indexPage : function () {
			pages.html(this.indexPageContent);

			if (pages.find("#slider").length) {
				var slider = pages.find("#slider");
				slider.slider(this.sliderConf);
				pages.find("#amountFrom").val(slider.slider("value"));
				pages.find("#amountTo").val(slider.slider("value") + 2);
			}
		},
		votePage : function (page, data) {
			pages.html(Handlebars.compile(page)({ question : data.question }));
		},
		finishPage : function (page, data) {
			pages.html(Handlebars.compile(page)({ result : data }));
		},
		errorPage : function (page, data) {},

		// голосовать
		vote : function (data) {
			$.post(location.origin+"/ajax/vote", data, function(resp) {
				if (!in_array(this.steps, resp.page))
					throw new Error('Непонятная ошибка...');

				if (this.curPage == 'index')
					this.indexPageContent = $("#pages").html();

				this.curPage = resp.page;
				this.points = resp.points;
				this.viewYears = resp.viewYears;

				this[resp.page+'Page']($("#page-"+resp.page).html(),resp);
				this.curPage = resp.page;
			}.bind(this));
		},
		// вернуться на начальную страницу / пройти тест еще раз
		goToHome : function () {
			deleteCookie('viewYears');
			deleteCookie('points');
			app.indexPage();
		}
	};

	var app = new voteApp(),
		pages = $("#pages");

	app.init();


	/**
	 * Music On/Off
	 */
	$( ".music-button #cfirst" ).on('click', function(){
		var audio = document.getElementById('music');
		if (this.checked)
			app.setMuteOn(audio);
		else
			app.setMuteOff(audio);
		setCookie('mute', this.checked, {expires: 86400 * 10});
	});



	/**
	 * Нажатие на кнопку "Проверить"
	 */
	pages.on('click','#page-index form input[type=submit]',function(e){
		e.preventDefault();
		var form = $(this).closest('form'),
			data = form.serialize(),
			name = $(this).data('name');

		if (name) data +='&'+name+'=1';
		app.vote(data);
	});

	/**
	 * Голосовалка
	 */

	pages.on('click','.image-block_image',function(e){
		e.preventDefault();

		var block = $(this).closest('.image-block'),
			data = {
				answer : $(this).data('answer-id'),
				points : app.points,
				viewYears : app.viewYears
			};
		app.vote(data);
	});


	/**
	 * Начачь заного
	 */
	pages.on('click', '.finish-share_link', function(e){
		e.preventDefault();
		app.goToHome();
	});


	/**
	 * Share
	 */
	pages.on('click', '.finish-social a', function(e){
		e.preventDefault();
		var social = $(this).closest('.finish-social'),
			title = social.data('title'),
			desc = social.data('desc'),
			img = social.data('img'),
			url = social.data('url')
			;
		if ($(this).hasClass('social-vk'))
			Share.vkontakte(url,title,img,desc);
		else if ($(this).hasClass('social-fb'))
			Share.facebook_dlg(url,title,img,desc);
		else if ($(this).hasClass('social-tw'))
			Share.twitter(url,title +' '+desc);
	});

	if ($( "#slider").length) {
		$( "#slider" ).slider(app.sliderConf);
		$("#amountFrom").val($("#slider").slider("value"));
		$("#amountTo").val($("#slider").slider("value") + 2);
	}



	/**
	 * Удалить ответ
	 */
	$('#answer-delete').on('click',function(){
		var id = $(this).data('id');
		if (!id || !confirm('Удалить ответ?'))
			return;
		window.location = location.origin+"/questions/answerDelete/"+id;
	});

	$('#QuestionsController-grid .question-delete').on('click',function(){
		var id = $(this).data('id');
		if (!id || !confirm('Удалить вопрос вместе с ответами?'))
			return;
		window.location = location.origin+"/questions/delete/"+id;
	});



});

