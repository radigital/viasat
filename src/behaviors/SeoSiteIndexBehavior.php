<?php

/**
 * SEO-штучки для главной страницы
 */
class SeoSiteIndexBehavior extends CBehavior {
	public function attach($owner) {
		parent::attach($owner);

		$seo = $owner->asa('seo');

		if (empty($seo))
			return;

		$seo->addMetaProperty('vk:title', app()->params->seo['title']);
		$seo->addMetaProperty('og:title', app()->params->seo['title']);
		$seo->addMetaProperty('og:description', app()->params->seo['desc']);
		$seo->addMetaProperty('fb:app_id', app()->params->fb_app_id);

		$seo->metaTitle = app()->params->seo['title'];
		$seo->metaDescription = app()->params->seo['desc'];

	}
}