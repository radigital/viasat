<?php

	/**
	 * Class ImageBehavior
	 *
	 * Поведение для показа картинки из бд
	 */
	class ImageBehavior extends CActiveRecordBehavior
	{

		public $_imagePath = null;
		public $imageField = null;

		public $filenameMask = null;

		public function getImageUrl(){
			return $this->imagePath . '/' . $this->filename;
		}

		public function getImagePath(){
			if ($this->_imagePath === null)
				$this->_imagePath = 'assets/images/'. strtolower(get_class($this->owner));

			return app()->createAbsoluteUrl($this->_imagePath);
		}

		public function setImagePath($path){
			$this->_imagePath = $path;
		}

		public function getFilename(){
			if (!$this->filenameMask || !is_array($this->filenameMask) || empty($this->filenameMask))
				return $this->owner->{$this->imageField};
			$owner = $this->owner;
			return array_reduce($this->filenameMask, function($prev, $next) use ($owner) {
				if (empty($prev))
					return $next;
				if ($owner->hasAttribute($prev))
					$prev = $owner->{$prev};
				if ($owner->hasAttribute($next))
					$next = $owner->{$next};
				return $prev.$next;
			});
		}

	}