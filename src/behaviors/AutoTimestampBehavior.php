<?php

	/**
	 * Class AutoTimestampBehavior
	 *
	 * Автоподстановка даты создания и изменения модели
	 */
	class AutoTimestampBehavior extends CActiveRecordBehavior {

		/**
		 * Имя поля, хранящего время создания модели.
		 */
		public $created = 'dateCreated';
		/**
		 * Имя поля, хранящего время изменения модели.
		 */
		public $modified = 'dateUpdate';


		public function beforeValidate($on) {
			if ($this->Owner->isNewRecord && isset($this->Owner->{$this->created}))
				$this->Owner->{$this->created} = new CDbExpression('NOW()');
			else
				if (isset($this->Owner->{$this->modified}))
					$this->Owner->{$this->modified} = new CDbExpression('NOW()');

			return true;
		}
	}