<?php

class SeoControllerBehavior extends CBehavior {

	/**
	 * @var string meta title страницы
	 */
	public $metaTitle;

	/**
	 * @var string meta description
	 */
	public $metaDescription;

	/**
	 * @var string meta keywords
	 */
	public $metaKeywords;

	/**
	 * @var array <meta property="" content="" />
	 */
	public $metaProperties = [ ];

	/**
	 * @var string link rel canonical
	 */
	public $canonical;

	/**
	 * Добавление meta-свойства
	 * @param name string property="$name"
	 * @param content string content="$string"
	 */
	public function addMetaProperty($name, $content) {
		$this->metaProperties[$name] = $content;
	}

	public function attach($owner) {
		parent::attach($owner);

		$this->addMetaProperty('og:locale', "ru_RU");
		$this->addMetaProperty('fb:app_id', app()->params->fb_app_id);
	}
}