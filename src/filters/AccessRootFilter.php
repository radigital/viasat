<?php

class AccessRootFilter extends CFilter
{
	protected function preFilter($filterChain)
	{
		if (!app()->user->isRoot) {
			app()->request->redirect(app()->request->getBaseUrl(true));
		}
        $filterChain->run();
	}
}