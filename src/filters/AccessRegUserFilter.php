<?php

class AccessRegUserFilter extends CFilter
{
	// куда редирект - на хост или на контроллер. По умолчанию на контроллер
	// controller||host
	public $redirect = 'controller';

	protected function preFilter($filterChain)
	{
		if (app()->user->isGuest) {
			app()->request->redirect(app()->request->getBaseUrl(true).($this->redirect =='controller' ? '/'.$filterChain->controller->id:''));
		}
        $filterChain->run();
	}
}