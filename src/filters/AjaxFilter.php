<?php

class AjaxFilter extends CFilter
{
	public $textError = 'Ошибка запроса';

	protected function preFilter($filterChain)
	{
        if (preg_match('/^ajax([\w\d]+)/',$filterChain->action->id)) {
            if (!app()->request->isAjaxRequest)
                $filterChain->controller->renderJSON(['error'=>$this->textError]);
        }
        $filterChain->run();
	}
}