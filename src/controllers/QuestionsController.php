<?php

class QuestionsController extends Controller
{

	public function filters() {
		return array_merge(parent::filters(),[
			[
				'application.filters.AccessRootFilter'
			]
		]);
	}

	/**
	 * Список
	 */
	public function actionIndex($page = 1)
	{
		$model = new Question;
		$model->setAttribute('year',Question::YEARFROM);

		$this->render('index',[
			'model' => $model,
			'page' => $page
		]);
	}

	public function actionAdd()	{
		$model = new Question;
		if ($this->populate($_POST, $model)){
			if ($model->validate()) {
				$model->userId=app()->user->id;
				$model->dateAdd = new CDbExpression('NOW()');
				if (isset($_POST['yearFrom']))
					$model->yearFrom = Question::getYearsFrom()[$_POST['yearFrom']];
				$model->insert();
			}
		}
		$this->redirect(url('questions'));
	}

	public function actionPullAdd($questionId=null) {
		$model = new Pull;
		if ($this->populate($_POST, $model)) {
			if ($model->validate()) {

				$model->setAttribute('userId', app()->user->id);
				$model->setAttribute('dateAdd', new CDbExpression('NOW()'));
				if (!$model->save()) {
					throw new CException('Не могу сохранить запись');
				}
				$answer1 = new Answer;
				$answer2 = new Answer;
				$answer3 = new Answer;

				$answer1->name = $model->answer1;
				$answer2->name = $model->answer2;
				$answer3->name = $model->answer3;

				if ($model->wrong==1) $answer2->wrong = $answer3->wrong = 'N';
				if ($model->wrong==2) $answer1->wrong = $answer3->wrong = 'N';
				if ($model->wrong==3) $answer2->wrong = $answer1->wrong = 'N';

				$answer1->pullId = $answer2->pullId = $answer3->pullId = $model->id;
				$answer1->dateAdd = $answer2->dateAdd = $answer3->dateAdd = new CDbExpression('now()');


				$path = Yii::getPathOfAlias('assets.images.answers');
				if (!file_exists($path))
					@mkdir($path, 0755);

				if ($file1 = CUploadedFile::getInstance($model,'file1')) {
					$filename = $path . '/' . $file1->name;
					$file1->saveAs($filename);
					if ($file1->hasError) throw new CException('Не могу загрузить файл 1');
					$ext = CFileHelper::getExtension($filename);
					$answer1->setAttribute('image', $ext);
				}

				if ($answer1->save()) {
					if ($file1)
						if (!rename($path . '/' . $file1->name, $path . '/' . $answer1->id . '.' . $ext))
							throw new CException('Не могу создать правильный файл');
				} else {
					if ($file1) {
						@unlink($path . '/' . $file1->name);
						throw new CException('Не могу сохранить запись');
					}
				}

				if ($file2 = CUploadedFile::getInstance($model,'file2')) {
					$filename = $path . '/' . $file2->name;
					$file2->saveAs($filename);
					if ($file2->hasError) throw new CException('Не могу загрузить файл 2');
					$ext = CFileHelper::getExtension($filename);
					$answer2->setAttribute('image', $ext);
				}

				if ($answer2->save()) {
					if ($file2)
						if (!rename($path . '/' . $file2->name, $path . '/' . $answer2->id . '.' . $ext))
							throw new CException('Не могу создать правильный файл');
				} else {
					if ($file2) {
						@unlink($path . '/' . $file2->name);
						throw new CException('Не могу сохранить запись');
					}
				}

				if ($file3 = CUploadedFile::getInstance($model,'file3')) {
					$filename = $path . '/' . $file3->name;
					$file3->saveAs($filename);
					if ($file3->hasError) throw new CException('Не могу загрузить файл 3');
					$ext = CFileHelper::getExtension($filename);
					$answer3->setAttribute('image', $ext);
				}

				if ($answer3->save()) {
					if ($file3)
						if (!rename($path . '/' . $file3->name, $path . '/' . $answer3->id . '.' . $ext))
							throw new CException('Не могу создать правильный файл');
				} else {
					if ($file3) {
						@unlink($path . '/' . $file3->name);
						throw new CException('Не могу сохранить запись');
					}
				}


				$this->redirect(url('questions'));
			}
		}

		$this->render('pullAdd',[
			'model' => $model,
			'questionId' => $questionId
		]);
	}

	public function actionAnswerEdit($id) {
		if (!$id || !$model = Answer::model()->findByPk($id))
			throw new CHttpException(404, 'Нет такого ответа');

		if ($this->populate($_POST, $model)) {

			if ($model->validate()) {
				$path = Yii::getPathOfAlias('assets.images.answers');
				$ext = $model->image;

				if ($model->file = CUploadedFile::getInstance($model,'file')) {
					if (!file_exists($path))
						@mkdir($path, 0755);
					$filename = $path . '/' . $model->file->name;

					$model->file->saveAs($filename);

					if ($model->file->hasError)
						throw new CException('Не могу загрузить файл');
					$ext = CFileHelper::getExtension($path.'/'.$model->file->name);
					$model->setAttribute('image', $ext);
				}

				$model->setAttribute('userId', app()->user->id);
				$model->setAttribute('dateAdd', new CDbExpression('NOW()'));


				if ($model->save()) {
					if ($model->file)
						if (!rename($path.'/'.$model->file->name, $path.'/'.$model->id.'.'.$ext))
							throw new CException('Не могу создать правильный файл');

					$this->redirect(url('questions'));
				} else {
					if ($model->file) {
						@unlink($path . '/' . $model->file->name);
						throw new CException('Не могу сохранить запись');
					}
				}
			}
		}

		$this->render('answerAdd',[
			'model' => $model,
			'questionId' => $model->questionId
		]);

	}

	public function actionDelete($id) {
		if ($question = Question::model()->findByPk($id)) {

			foreach ($question->pulls as $pull) {
				foreach ($pull->answers as $answer) {
					if ($answer->image) {
						@unlink(Yii::getPathOfAlias('assets.images.answers') . '/' . $answer->id . '.' . $answer->image);
					}
					$answer->delete();
				}
				$pull->delete();
			}

			$question->delete();
		}

		$this->redirect(url('questions'));
	}


	public function actionQuestionDelete($id) {
		if ($question = Question::model()->findByPk($id)) {
			foreach ($question->answers as $answer) {
				if ($answer->image) {
					@unlink(Yii::getPathOfAlias('assets.images.answers') . '/' . $answer->id . '.' . $answer->image);
				}
				Answer::model()->deleteByPk($answer->id);
			}

			Question::model()->deleteByPk($id);
			$res['success'] = 'удалено успешно';
		} else {
			$res['error'] = 'Нет такого вопроса';
		}
		$this->renderJson($res);
	}

	public function actionAnswer($id) {
		if (!$id || !$model = Answer::model()->findByPk($id))
			throw new CHttpException(404,'Нет такого ответа');

		if ($this->populate($_POST, $model)) {

			if ($model->validate()) {
				$path = Yii::getPathOfAlias('assets.images.answers');
				$ext = $model->image;

				if ($model->file = CUploadedFile::getInstance($model,'file')) {
					if (!file_exists($path))
						@mkdir($path, 0755);
					$filename = $path . '/' . $model->file->name;

					$model->file->saveAs($filename);

					if ($model->file->hasError)
						throw new CException('Не могу загрузить файл');
					$ext = CFileHelper::getExtension($path.'/'.$model->file->name);
					$model->setAttribute('image', $ext);
				}

				$model->setAttribute('userId', app()->user->id);
				$model->setAttribute('dateAdd', new CDbExpression('NOW()'));


				if ($model->save()) {
					if ($model->file)
						if (!rename($path.'/'.$model->file->name, $path.'/'.$model->id.'.'.$ext))
							throw new CException('Не могу создать правильный файл');

					$this->redirect(url('questions'));
				} else {
					if ($model->file) {
						@unlink($path . '/' . $model->file->name);
						throw new CException('Не могу сохранить запись');
					}
				}
			}
		}

		$this->render('answer',[
			'answer' => $model
		]);
	}


}