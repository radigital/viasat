<?php

class LoginController extends Controller{

	public function actionIndex()
	{
		$this->render('login',[]);
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionExtauth(){
		$service = app()->request->getParam('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);

			$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
			$authIdentity->cancelUrl = $this->createAbsoluteUrl('login');

			if ($authIdentity->authenticate()) {
				$identity = new UserIdentity($authIdentity);

				// Успешный вход
				if ($identity->authenticate()) {
					app()->user->login($identity,3600*24*7);

					// Специальный редирект с закрытием popup окна
					$authIdentity->redirect();
				}
				else {
					// Закрываем popup окно и перенаправляем на cancelUrl
					$authIdentity->cancel();
				}
			}

			// // Что-то пошло не так, перенаправляем на страницу входа
			// $this->redirect(array('login'));
		}
	}
}