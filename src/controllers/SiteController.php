<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return [
			'page'=>[
				'class'=>'CViewAction',
			],
		];
	}

	public function behaviors() {
		return CMap::mergeArray(parent::behaviors(), [
			'seo' => 'SeoControllerBehavior',
		]);
	}

	/**
	 * Главная страница
	 */
	public function actionIndex()
	{
		$this->attachBehavior('seoIndex', new SeoSiteIndexBehavior);
		$this->isMainpage = true;

		$voter = new Voter();
		$data = $voter->getArrayData();

		$this->render('index', $data);
	}


}