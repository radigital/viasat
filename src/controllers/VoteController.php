<?php

class VoteController extends Controller
{

	public function behaviors() {
		return CMap::mergeArray(parent::behaviors(), [
			'seo' => 'SeoControllerBehavior'
		]);
	}


	public function actionAjaxIndex() {
		$amountFrom = null;
		if (isset($_POST['main'])) {
			$amountFrom  = (int)$_POST['amountFrom'];
		}
		$voter = new Voter($amountFrom, 'vote');
		$data = $voter->getArrayData();
		$this->renderJson($data);
	}

}