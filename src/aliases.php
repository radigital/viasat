<?php

Yii::setPathOfAlias('root', realpath(dirname(__FILE__).'/..'));
Yii::setPathOfAlias('assets', realpath(dirname(__FILE__) . '/../assets'));
Yii::setPathOfAlias('assets.images', realpath(dirname(__FILE__) . '/../assets/images'));
Yii::setPathOfAlias('assets.images.answers', realpath(dirname(__FILE__) . '/../assets/images/answers'));
