<?php

	setlocale(LC_CTYPE, 'UTF-8');

	require_once(dirname(__FILE__) . '/helpers/helpers.php');

	$config = require_once(__DIR__ . '/config/main.php');
	$localConfigFilename = __DIR__ . '/config/local.php';

	if (file_exists($localConfigFilename))
		$config = mergeConfigArray($config, require($localConfigFilename));

	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG', isset($config['params']['dev']) ? $config['params']['dev'] : false );
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

	//$loader = require(__DIR__ . '/../vendor/autoload.php');

	if (!YII_DEBUG) {
		$yii = __DIR__.'/../vendor/yiisoft/yii/framework/yiilite.php';
	} else {
		$yii = __DIR__.'/../vendor/yiisoft/yii/framework/yii.php';
	}

	require_once($yii);

	Yii::$enableIncludePath = false;
	include 'aliases.php';

	Yii::createWebApplication($config)->run();