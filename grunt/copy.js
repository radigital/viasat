module.exports =  {
	images: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.images %>',
		src: [ '**' ],
		dest: '<%= dirs.assets %>/images'
	},
	fonts: {
		nonull: true,
		expand: true,
		cwd: '<%= dirs.fonts %>',
		src: [ '**' ],
		dest: '<%= dirs.assets %>/fonts'
	},
	fonts_bs: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.vendor %>/twbs/bootstrap/fonts",
		src: [ '**' ],
		dest: '<%= dirs.assets %>/fonts'
	},
	music: {
		nonull: true,
		expand: true,
		cwd: "<%= dirs.music %>",
		src: [ '**' ],
		dest: '<%= dirs.assets %>/music'
	}
};