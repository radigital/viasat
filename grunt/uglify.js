module.exports =  {
	libs: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.libs %>',
		dest: '<%= dirs.assets %>/js/libs.min.js'
	},
	app: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.app %>',
		dest: '<%= dirs.assets %>/js/app.min.js'
	},
	extra: {
		options: {
			sourceMap: true
		},
		src: '<%= scripts.extra %>',
		dest: '<%= dirs.assets %>/js/extra.min.js'
	},

	all: {
		options: {
			sourceMap: true
		},
		src: [
			'<%= scripts.extra %>',
			'<%= scripts.libs %>',
			'<%= scripts.app %>'
		],
		dest: '<%= dirs.assets %>/js/js.min.js'
	}

};