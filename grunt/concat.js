module.exports = {
	dist: {
		src: [
			'<%= dirs.jslib %>/swfobject.js',
			"<%= dirs.jslib %>/html5shiv.js",
			"<%= dirs.vendor %>/jquery/jquery.js",
			"<%= dirs.jslib %>/jquery-ui.js",
			"<%= dirs.jslib %>/handlebars.v2.js",
			"<%= dirs.vendor %>/twbs/bootstrap/js/dropdown.js"
		],
		dest: '<%= dirs.assets %>/js/libs.min.js'
	},
	app: {
		src: [
			"<%= dirs.jsapp %>/share.js",
			"<%= dirs.jsapp %>/app.js"
		],
		dest: '<%= dirs.assets %>/js/app.min.js'
	},
	extra: {
		src: [
			"<%= dirs.jsextra %>/*"
		],
		dest: '<%= dirs.assets %>/js/extra.min.js'
	}
};