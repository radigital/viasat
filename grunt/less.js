module.exports =  {
	prod: {
		options: {
			compress	: true,
			cleancss	: true

		},
		src    : [
			'<%= dirs.less %>/styles.less'
		],
		dest   : "<%= dirs.assets %>/css/styles.css"
	},
	prod_adm: {
		options: {
			compress	: true,
			cleancss	: true

		},
		src    : [
			'<%= dirs.less %>/stylesAdm.less'
		],
		dest   : "<%= dirs.assets %>/css/stylesAdm.css"
	},

	dev: {
		src    : '<%= dirs.less %>/styles.less',
		dest   : "<%= dirs.assets %>/css/styles.css"
	},
	dev_adm: {
		src    : '<%= dirs.less %>/stylesAdm.less',
		dest   : "<%= dirs.assets %>/css/stylesAdm.css"
	}
};