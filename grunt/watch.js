module.exports =  {
	scripts: {
		files: [
			'<%= dirs.jsapp %>/*.js',
			'<%= dirs.less %>/*.less',
		],
		tasks: ['newer:uglify:app','newer:less:dev']
	}
};